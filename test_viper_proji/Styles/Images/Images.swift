//
//  Images.swift
//  test_viper_proji
//
//  Created by Nikita Shumilin on 28.10.2021.
//

import UIKit

extension Style.Images {
    static let star = UIImage.init(named: "star")
    static let starFill = UIImage.init(named: "star.fill")
    
    static let film = UIImage.init(named: "film")
}
