//
//  MainRouter.swift
//  test_viper_proji
//
//  Created by Nikita Shumilin on 24.10.2021.
//

class MainRouter: MainRouterInput {

    deinit {
        print("\(type(of: self)) - \(#function)")
    }
}
